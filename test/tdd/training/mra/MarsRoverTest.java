package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testMarsRover() throws MarsRoverException {
		List<String> list = new ArrayList<String>();
		list.add("(4,7)");
		list.add("(2,3)");
		MarsRover mr = new MarsRover(10, 10, list);
		assertTrue(mr.planetContainsObstacleAt(4, 7));
	}

	@Test
	public void testEmptyCommand() throws MarsRoverException {
		List<String> list = new ArrayList<String>();
		list.add("(4,7)");
		list.add("(2,3)");
		MarsRover mr = new MarsRover(10, 10, list);
		assertEquals("(0,0,N)", mr.executeCommand(""));
	}

	@Test
	public void testRotationRover() throws MarsRoverException {
		List<String> list = new ArrayList<String>();
		list.add("(4,7)");
		list.add("(2,3)");
		MarsRover mr = new MarsRover(10, 10, list);
		assertEquals("(0,0,E)", mr.executeCommand("r"));
	}

	@Test
	public void testMovingForward() throws MarsRoverException {
		List<String> list = new ArrayList<String>();
		list.add("(4,7)");
		list.add("(2,3)");
		MarsRover mr = new MarsRover(10, 10, list);
		mr.executeCommand("r");
		mr.executeCommand("f");
		mr.executeCommand("f");
		mr.executeCommand("f");
		mr.executeCommand("f");
		mr.executeCommand("f");
		mr.executeCommand("f");
		mr.executeCommand("f");
		mr.executeCommand("l");
		mr.executeCommand("f");
		mr.executeCommand("f");
		mr.executeCommand("f");
		mr.executeCommand("f");
		mr.executeCommand("f");
		mr.executeCommand("f");
		assertEquals("(7,7,N)", mr.executeCommand("f"));
	}

	@Test
	public void testMovingBackword() throws MarsRoverException {
		List<String> list = new ArrayList<String>();
		list.add("(4,7)");
		list.add("(2,3)");
		MarsRover mr = new MarsRover(10, 10, list);
		mr.executeCommand("f");
		mr.executeCommand("f");
		mr.executeCommand("f");
		mr.executeCommand("f");
		mr.executeCommand("f");
		mr.executeCommand("f");
		mr.executeCommand("f");
		mr.executeCommand("f");
		mr.executeCommand("r");
		mr.executeCommand("f");
		mr.executeCommand("f");
		mr.executeCommand("f");
		mr.executeCommand("f");
		mr.executeCommand("f");
		assertEquals("(4,8,E)", mr.executeCommand("b"));

	}

	@Test
	public void testCombinedCommand() throws MarsRoverException {
		List<String> list = new ArrayList<String>();
		MarsRover mr = new MarsRover(10, 10, list);
		assertEquals("(2,2,E)", mr.executeCommand("ffrff"));

	}

	@Test
	public void testPlanetEdge() throws MarsRoverException {
		List<String> list = new ArrayList<String>();
		MarsRover mr = new MarsRover(10, 10, list);
		assertEquals("(0,9,N)", mr.executeCommand("b"));

	}

	@Test
	public void testSingleObstacle() throws MarsRoverException {
		List<String> list = new ArrayList<String>();
		list.add("(2,2)");
		MarsRover mr = new MarsRover(10, 10, list);
		assertEquals("(1,2,E)(2,2)", mr.executeCommand("ffrfff"));

	}

	@Test
	public void testMultipleObstacles() throws MarsRoverException {
		List<String> list = new ArrayList<String>();
		list.add("(2,2)");
		list.add("(2,1)");
		MarsRover mr = new MarsRover(10, 10, list);
		assertEquals("(1,1,E)(2,2)(2,1)", mr.executeCommand("ffrfffrflf"));
	}
	
	@Test
	public void testEdgeObstacle() throws MarsRoverException {
		List<String> list = new ArrayList<String>();
		list.add("(0,9)");
		MarsRover mr = new MarsRover(10, 10, list);
		assertEquals("(0,0,N)(0,9)", mr.executeCommand("b"));
	}
	
	@Test
	public void testTour() throws MarsRoverException {
		List<String> list = new ArrayList<String>();
		list.add("(2,2)");
		list.add("(0,5)");
		list.add("(5,0)");
		MarsRover mr = new MarsRover(6, 6, list);
		assertEquals("(0,0,N)(2,2)(0,5)(5,0)", mr.executeCommand("ffrfffrbbblllfrfrbbl"));
	}

}
