package tdd.training.mra;

import java.util.List;

public class MarsRover {
	private int roverX;
	private int roverY;
	private String roverDirection;
	private int planetX;
	private int planetY;
	private List<String> planetObstacles;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX = planetX;
		this.planetY = planetY;
		this.planetObstacles = planetObstacles;
		roverX = 0;
		roverY = 0;
		roverDirection = "N";
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		return planetObstacles.contains("(" + x + "," + y + ")");
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		String output = "";
		for (int i = 0; i < commandString.length(); i++) {
			int tempX = roverX;
			int tempY = roverY;
			switch (commandString.charAt(i)) {
			case 'r':
				rotateRight();
				break;
			case 'l':
				rotateLeft();
				break;
			case 'f':
				forward();
				break;
			case 'b':
				backward();
				break;
			}
			if (this.planetContainsObstacleAt(roverX, roverY)) {
				if (!output.contains("(" + roverX + "," + roverY + ")"))
					output = output.concat("(" + roverX + "," + roverY + ")");
				roverX = tempX;
				roverY = tempY;
			}
		}
		return "(" + roverX + "," + roverY + "," + roverDirection + ")".concat(output);
	}
	private void backward() throws MarsRoverException {
		switch (roverDirection) {
		case "N":
			roverY--;
			if (roverY < 0) {
				roverY = planetY - 1;
			}
			break;
		case "S":
			roverY++;
			if (roverY > planetY - 1) {
				roverY = 0;
			}
			break;
		case "W":
			roverX++;
			if (roverX > planetX - 1) {
				roverX = 0;
			}
			break;
		case "E":
			roverX--;
			if (roverX < 0) {
				roverX = planetX - 1;
			}
			break;
		default:
			throw new MarsRoverException();
		}
	}
	private void forward() throws MarsRoverException {
		switch (roverDirection) {
		case "N":
			roverY++;
			if (roverY > planetY - 1) {
				roverY = 0;
			}
			break;
		case "S":
			roverY--;
			if (roverY < 0) {
				roverY = planetY - 1;
			}
			break;
		case "W":
			roverX--;
			if (roverX < 0) {
				roverX = planetX - 1;
			}
			break;
		case "E":
			roverX++;
			if (roverX > planetX - 1) {
				roverX = 0;
			}
			break;
		default:
			throw new MarsRoverException();
		}
	}
	public void rotateRight() throws MarsRoverException {
		switch (roverDirection) {
		case "N":
			roverDirection = "E";
			break;
		case "S":
			roverDirection = "W";
			break;
		case "W":
			roverDirection = "N";
			break;
		case "E":
			roverDirection = "S";
			break;
		default:
			throw new MarsRoverException();
		}
	}
	public void rotateLeft() throws MarsRoverException {
		switch (roverDirection) {
		case "N":
			roverDirection = "W";
			break;
		case "S":
			roverDirection = "E";
			break;
		case "W":
			roverDirection = "S";
			break;
		case "E":
			roverDirection = "N";
			break;
		default:
			throw new MarsRoverException();
		}
	}
}
